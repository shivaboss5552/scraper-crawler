console.log("Working!");
next_button = document.querySelector("#next button");

function generate(){
  website_type = document.querySelector("#website_type").value
  parent = document.querySelector("#main-form");
  refresh_button = document.createElement("div");
  refresh_button.setAttribute('class','sub-container');
  refresh_button.innerHTML = `<input type="text" id="test-url" autocomplete="off" placeholder ="Enter the test URL">
  <button type="button">Check!</button>`;
  parent.appendChild(refresh_button);

  if (website_type == "event"){
    fields = ["@type","title","venue","start_date","start_time","end_date","end_time","latitude","longitude","location","website","terms_condition","contact_name","contact_number","email_address","description","cost","currency","tag","profile_image"]

  }
  else if (website_type == "job") {
    fields = ["@type","title","job_location","cmp_name","employment_type","min_exp","max_exp","description","profile_image","preferred_qualification","skill","latitude","longitude","tags","external_link_to_apply","application_deadline","phone_number","email_address","website","url","about_us","contact_us","how_to_apply","telephone_number"]
  }

  else if (website_type == "meetup"){
    fields = ["@type","title","venue","start_date","start_time","end_date","end_time","latitude","longitude","location","website","terms_condition","contact_name","contact_number","email_address","description","cost","currency","tag","profile_image"]
  }

  for(var i=0;i<fields.length;i++){
    console.log(fields[i]);
    block = document.createElement("div");
    block.setAttribute('class','sub-container sent-data')
    block.innerHTML = `<label for="${fields[i]}">${fields[i]}</label>
      <input type="text" id="${fields[i]}" autocomplete="off">`;
    parent.appendChild(block);
  }
  submit_button = document.createElement("div");
  submit_button.setAttribute('id','submit');
  submit_button.setAttribute('class','sub-container');
  submit_button.innerHTML = '<button type="button">Save</button>';
  parent.appendChild(submit_button)
  parent.removeChild(next_button.parentElement);

  /*save form*/
  final_button = document.querySelector("#submit button");
  console.log(final_button,"final_button")
  if(final_button!=null){
    function save(){
      w_name = document.querySelector("#name");
      w_url = document.querySelector("#w-url");
      w_regex = document.querySelector("#regex");
      w_type = document.querySelector("#website_type")
      if(w_name.value == "" || w_url.value == "" || w_regex.value == ""){
        alert("Fill all mandatory details before saving!");
      }
      else{
        console.log("Form Saved!");
        var detailsJSON = {"name":w_name.value,"url":w_url.value,"regex":w_regex.value,"type":w_type.value}

        mapping_data = document.querySelectorAll(".sent-data input");
        mapping_container = {}
        for(var i = 0; i<mapping_data.length;i++){
          mapping_container[mapping_data[i].id] = mapping_data[i].value
        }
        detailsJSON["mapping"] = mapping_container

        detailsJSON = JSON.stringify(detailsJSON)
        console.log(detailsJSON);

        /*Saving xhr*/
        var xhr = new XMLHttpRequest();
        xhr.open("POST","http://"+ window.location.hostname+":8000/admin/save/",true);
        var data = new FormData();
        data.append("saving_details",detailsJSON)

        xhr.onload = function(){
          if (this.status == 200){
            if (this.responseText == "1"){
              alert("New Website Added Successfully!")
              location.reload(true);
            }
            else if (this.responseText == "2"){
              alert("Website Updated Successfully!")
              location.reload(true);
            }
            else if (this.responseText == "0"){
              alert("Something went wrong!")
            }

          }
        }

        xhr.send(data)

      }

    }
    final_button.addEventListener("click",save)
  }



  function fetch(){
    console.log("fetch");
    var data = new FormData();
    data.append("base-url",document.querySelector("#test-url").value)
    possible = document.querySelectorAll(".sent-data");
    for(var i=0;i<possible.length;i++){
      key = possible[i].querySelector("label").textContent
      value = possible[i].querySelector("input").value
      console.log(key,value)
      data.append(key,value)
    }

    var xhr = new XMLHttpRequest();
    xhr.open("POST","http://"+window.location.hostname+":8000/admin/testboard/",true);
    xhr.onload = function (){
      /*spring cleaning*/
      old = document.querySelectorAll(".received-data")
      parent = document.querySelector("#main-form")
      if(old.length != 0){
        for(var i=0;i<old.length;i++){
          parent.removeChild(old[i])
        }
      }
      console.log(this.responseText);
      result = JSON.parse(this.responseText)
      console.log(result);
      for (let key in result){
       if(result.hasOwnProperty(key)){
         console.log(`${key} : ${result[key]}`)
         current_element = document.querySelector("#"+key).parentElement;
         new_element = document.createElement("div");
         new_element.setAttribute('class','sub-container received-data')
         new_element.innerHTML = result[key]
         current_element.after(new_element);
          }
        }



    }

    xhr.send(data);

  }

  refresh = document.querySelector("#test-url+button")
  refresh.addEventListener("click",fetch)


}
next_button.addEventListener("click",generate)






function dropdown(){
  console.log(this.value);
  current_value = this.value
  if (current_value != "new"){
    console.log("trigger");
    var xhr = new XMLHttpRequest();
    xhr.open("GET","http://"+window.location.hostname+":8000/admin/autofill/?name="+current_value,true);
    xhr.onload = function(){
      if (this.status == 200){
        console.log(this.responseText);
        result = JSON.parse(this.responseText);
        document.querySelector("#name").value = result["website_title"];
        document.querySelector("#w-url").value = result["website_url"];
        document.querySelector("#regex").value = result["website_regex"];
        document.querySelector("#website_type").value = result["website_type"];


        /*Repeated with tweaks(Do spring cleaning)*/

        /*SPring Cleaning*/
        website_type = result["website_type"];
        parent = document.querySelector("#main-form");
        delete_upper = document.querySelector("#test-url");
        if (delete_upper != null){
          delete_upper = delete_upper.parentElement;
          parent.removeChild(delete_upper);

        }

        check1 = document.querySelectorAll(".sent-data,.received-data")
        console.log(check1.length,"Length")
        if (check1.length != 0){
          for(var i=0; i<check1.length;i++){
            parent.removeChild(check1[i])
            console.log(check1[i]);
          }
        }

        check2 = document.querySelector("#submit");
        if (check2 != null){
          parent.removeChild(check2);
        }

        next_button = document.querySelector("#next");
        if(next_button != null){
          parent.removeChild(next_button);
        }

        /*Spring Cleaning ends*/


        refresh_button = document.createElement("div");
        refresh_button.setAttribute('class','sub-container');
        refresh_button.innerHTML = `<input type="text" id="test-url" autocomplete="off" placeholder ="Enter the test URL">
        <button type="button">Check!</button>`;
        parent.appendChild(refresh_button);

        /*Middle Rows Generator*/

        if (website_type == "event"){
          fields = ["@type","title","venue","start_date","start_time","end_date","end_time","latitude","longitude","location","website","terms_condition","contact_name","contact_number","email_address","description","cost","currency","tag","profile_image","url"]

        }
        else if (website_type == "job") {
          fields = ["@type","title","job_location","cmp_name","employment_type","min_exp","max_exp","description","profile_image","preferred_qualification","skill","latitude","longitude","tags","external_link_to_apply","application_deadline","phone_number","email_address","website","url","about_us","contact_us","how_to_apply","telephone_number"]
        }

        else if (website_type == "meetup"){
          fields = ["@type","title","venue","start_date","start_time","end_date","end_time","latitude","longitude","location","website","terms_condition","contact_name","contact_number","email_address","description","cost","currency","tag","profile_image","url"]
        }

        for(var i=0;i<fields.length;i++){
          console.log(fields[i]);
          block = document.createElement("div");
          block.setAttribute('class','sub-container sent-data')
          block.innerHTML = `<label for="${fields[i]}">${fields[i]}</label>
            <input type="text" id="${fields[i]}" autocomplete="off">`;
          parent.appendChild(block);
        }
        /*submit Button generator*/
        submit_button = document.createElement("div");
        submit_button.setAttribute('id','submit');
        submit_button.setAttribute('class','sub-container');
        submit_button.innerHTML = '<button type="button">Save</button>';
        parent.appendChild(submit_button)

        /*save form*/
        final_button = document.querySelector("#submit button");
        console.log(final_button,"final_button")
        if(final_button!=null){
          function save(){
            w_name = document.querySelector("#name");
            w_url = document.querySelector("#w-url");
            w_regex = document.querySelector("#regex");
            w_type = document.querySelector("#website_type")
            if(w_name.value == "" || w_url.value == "" || w_regex.value == ""){
              alert("Fill all mandatory details before saving!");
            }
            else{
              console.log("Form Saved!");
              var detailsJSON = {"name":w_name.value,"url":w_url.value,"regex":w_regex.value,"type":w_type.value}

              mapping_data = document.querySelectorAll(".sent-data input");
              mapping_container = {}
              for(var i = 0; i<mapping_data.length;i++){
                mapping_container[mapping_data[i].id] = mapping_data[i].value
              }
              detailsJSON["mapping"] = mapping_container

              detailsJSON = JSON.stringify(detailsJSON)
              console.log(detailsJSON);

              /*Saving xhr*/
              var xhr = new XMLHttpRequest();
              xhr.open("POST","http://"+window.location.hostname+":8000/admin/save/",true);
              var data = new FormData();
              data.append("saving_details",detailsJSON)

              xhr.onload = function(){
                if (this.status == 200){
                  if (this.responseText == "1"){
                    alert("New Website Added Successfully!")
                    location.reload(true);
                  }
                  else if (this.responseText == "2"){
                    alert("Website Updated Successfully!")
                    location.reload(true);
                  }
                  else if (this.responseText == "0"){
                    alert("Something went wrong!")
                  }
                }
              }

              xhr.send(data)

            }




          }
          final_button.addEventListener("click",save)
        }



        /*Autofilling*/
        map_json = result["mapping"];
        for (let key in map_json){
         if(map_json.hasOwnProperty(key)){
           console.log(`${key} : ${map_json[key]}`)
           current_element = document.getElementById(key).value = map_json[key];

            }
          }


        /*repeated xhr*/
        function fetch(){
          console.log("fetch");
          var data = new FormData();
          data.append("base-url",document.querySelector("#test-url").value)
          possible = document.querySelectorAll(".sent-data");
          for(var i=0;i<possible.length;i++){
            key = possible[i].querySelector("label").textContent
            value = possible[i].querySelector("input").value
            console.log(key,value)
            data.append(key,value)
          }

          var xhr = new XMLHttpRequest();
          xhr.open("POST","http://"+window.location.hostname+":8000/admin/testboard/",true);
          xhr.onload = function (){
            /*spring cleaning*/
            old = document.querySelectorAll(".received-data")
            parent = document.querySelector("#main-form")
            if(old.length != 0){
              for(var i=0;i<old.length;i++){
                parent.removeChild(old[i])
              }
            }
            console.log(this.responseText);
            result = JSON.parse(this.responseText)
            console.log(result);
            for (let key in result){
             if(result.hasOwnProperty(key)){
               console.log(`${key} : ${result[key]}`)
               current_element = document.querySelector("#"+key).parentElement;
               new_element = document.createElement("div");
               new_element.setAttribute('class','sub-container received-data')
               new_element.innerHTML = result[key]
               current_element.after(new_element);
                }
              }



          }

          xhr.send(data);

        }

        refresh = document.querySelector("#test-url+button")
        refresh.addEventListener("click",fetch)



      }
    }

    xhr.send()



  }

}






support = document.querySelector("#website_support");
support.addEventListener("click",dropdown);
console.log(support.value)
