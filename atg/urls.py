
from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url

urlpatterns = [
    path('main/', include('taskOne.urls')),
    path('admin/', admin.site.urls),
    path('api/', include('API.urls'))
]
