from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os,time

def selenium_crawler():
    url = "https://www.udemy.com/course/microsoft-power-bi-up-running-with-power-bi-desktop/"
    """ Renders the url in the selenium and returns all hyperlinks """
    options = Options()
    options.add_argument("user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36")
    options.headless = True
    driver = webdriver.Chrome(os.path.join(os.getcwd(),"taskOne","driver","chromedriver"), options=options)
    driver.get(url)
    time.sleep(2)
    source = driver.page_source
    driver.quit()
    print(len(source))
    
selenium_crawler()
