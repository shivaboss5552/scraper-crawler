import mysql.connector as sql
from datetime import datetime
from collections import defaultdict
import base64
from json import loads
import requests
from operator import itemgetter as iget
import pprint

#Used to transfer data from middle database to live database.
def DbTransfer():
    try:
        db = sql.connect(host="scrapdb2.crbyejbc1y6i.ap-south-1.rds.amazonaws.com", user="root", passwd=str(
        base64.b64decode("OG5SeTdFYmNNbjVy"), "utf-8"), auth_plugin='mysql_native_password', database='scraping_middle_db')

        print("Importing....")
        data,all_id = list(),defaultdict(set)
        cursor = db.cursor()

        info = {"job":{"common_param": lambda arr : ('job',) + iget(1,2,4,11,5,6,25,30,31,26,27,28,29)(arr),
                       "feature_param":lambda arr : iget(32,3,10,8,14,7)(arr) + ('',)+iget(15,24,23,22,21,20,19,18,17,16,28,29,37)(arr),
                       "query":"""INSERT INTO p1036_post_job_details(post_id,cmp_name,job_location,skills,preferred_qualification,employment_type,min_exp,duration,application_deadline,hiring_process,how_to_apply,contact_us,about_us,url,website,email_address,phone_number,salary_range,created_at,updated_at,remote)
                        VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"""},
                
                "event":{"common_param": lambda arr: ('event',) + iget(1,2,19,24,25,26,27,10,11,28,29,30,31)(arr),
                         "feature_param": lambda arr: iget(4,5,6,7,15,16,17,12,3,13,14,30,31)(arr) ,
                         "query":"""INSERT INTO p1036_post_event_details(post_id,start_date,start_time,end_date,end_time,contact_name,contact_number,email_address,venue,city,website,terms_condition,created_at,updated_at)
                        VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"""},
                
                "education":{"common_param" : lambda arr: ('education',) + iget(1,2,4,13,11,12,18,16,17,19,20,26,27)(arr),
                             "feature_param":lambda arr: iget(3,21,14,15,26,27)(arr),
                             "query":"""INSERT INTO p1036_post_education_details(post_id,website,education_type,venue,city,created_at,updated_at)
                        VALUES(%s,%s,%s,%s,%s,%s,%s);"""}}
        
        for types in ("job","event","education"):
            cursor.execute(f'select * from middle_mst_{types} where traversed=0;')
            for f in cursor.fetchall():
                if f[-2]:
                    group_data = list(map(int,f[-2].split(",")))
                else:group_data = []
                all_id[types].add(str(f[0]))
                data.append(dict(key=f[0],common_param = info[types]['common_param'](f),feature_param=info[types]['feature_param'](f),feature=types,groups=group_data))

            if all_id[types]:
                cursor.execute(f"update middle_mst_{types} set traversed = 1 where id IN ({','.join(all_id[types])});")
    

        db.commit()

        if not data:
            print("No data to import!")
            return 0

        main_query = """INSERT INTO p1036_posts(type,user_id,title,description,tags,profile_image,cover_type,status,latitude,longitude,imported,import_url,created_at,updated_at)
                        VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"""

        group_query = """INSERT INTO p1036_post_groups(post_id,group_id,created_at,updated_at)
                        VALUES(%s,%s,%s,%s);"""


        
        
        db2 = sql.connect(host="aug1216.clb8pwlmio9e.ap-south-1.rds.amazonaws.com", user="root", passwd=str(
        base64.b64decode("OG5SeTdFYmNNbjVy"), "utf-8"), auth_plugin='mysql_native_password', database='atgworld_p1036')
        cursor2 = db2.cursor()


        for i in range(0,len(data)):
            if data[i]["common_param"][5]:
                data[i]["common_param"] = list(data[i]["common_param"])
                data[i]["common_param"][6] = 'uploaded'
                
            feature = data[i]["feature"] 
            cursor2.execute(main_query,data[i]["common_param"])
            post_id = cursor2.lastrowid
            data[i]["key"] = post_id
            cursor2.execute(info[feature]["query"],(post_id,)+data[i]["feature_param"])
            
        db2.commit()
        
        current = datetime.now()
        f_date = current.strftime('%Y-%m-%d %H:%M:%S')
        parameters = []
        for x in data:
            for group in x.get("groups"):
                parameters.append((x.get("key"),group,f_date,f_date))
                
        cursor2.executemany(group_query,parameters)


        db2.commit()

        
    
        print("Transfer completed successfully!")
        with open("middle_DB_transfer_log.txt","a") as logger:
            for log in data:
                msg = f'Imported {log["feature"]} with ATG id:{log["key"]} at {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}\n'
                logger.write(msg)
                print(msg)


        def send_data_to_API(data):
            print("Sending encountered phones and email to API...")
            groups = loads(open("group_data.txt","r").read().strip())
            groups = {int(key):value for key,value in groups.items()}
            for c in range(0,len(data)):
                scraped_data = dict()
                if data[c]["feature"] == "event":
                    scraped_data["email_address"],scraped_data["location"] = data[c]["parameters"][16],data[c]["parameters"][11]
                elif data[c]["feature"] == "job":
                    scraped_data["email_address"],scraped_data["phone_number"],scraped_data["job_location"] = data[c]["parameters"][17],data[c]["parameters"][16],data[c]["parameters"][2]
                if scraped_data.get("phone_number") or scraped_data.get("email_address"):
                    submit= dict(url=f"https://atg.world/view-{data[c]['feature']}/{data[c]['key']}")
                    if data[c].get("groups"):
                        submit["group_name"]=groups[data[c].get("groups")[0]][0]
                    else:
                        submit["group_name"] = "NULL"
                    if scraped_data.get("phone_number"):
                        submit["phone"] = scraped_data.get("phone_number")
                    if scraped_data.get("email_address"):
                        submit["email"] = scraped_data.get("email_address")
                    for d in ("job_location","location"):
                        if scraped_data.get(d):
                            submit["location"] = scraped_data.get(d)
                            break
                    try:
                        try:
                            ip = requests.get("https://textuploader.com/16tjk/raw").content.decode("utf-8")
                        except Exception as e:
                            ip = "13.71.83.193"
                            print(e)
                        sent = requests.post("http://"+ip+"/api/submit",data=submit)
                        with open("phone_and_email.txt","a") as API:
                            API.write(f"PHONE NUMBER:{submit.get('phone')} | EMAIL: {submit.get('email')} | URL: {submit.get('url')} | STATUS:{sent.content} | FEATURE: {submit.get('group_name')}\n\n")
                    except Exception as e:
                        print("Something went wrong with the API!",e)
            print("Done")

        #send_data_to_API(data)

        print(f"Total Imports : {len(data)}")
    except IndexError:
        with open("script_crash_log.txt","a") as crash:
            crash.write(e)

DbTransfer()
