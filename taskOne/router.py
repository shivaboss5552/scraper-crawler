from .models import Education,Event,Job

class DatabaseRouter:
    
    #Used to route middle DB model operations to middle DB.
    def db_for_read(self, model, **hints):
        
        if model in (Education, Event, Job):
            return 'middle'
        return None

    def db_for_write(self, model, **hints):
        
        if model in (Education, Event, Job):
            return 'middle'
        return None


    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if db == "middle":
            return False

        return True
