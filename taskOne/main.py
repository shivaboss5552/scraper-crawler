from taskOne.models import python_website_support, p_non_interesting_urls, p_interesting_urls,reboot_log
from .helpers import (selenium_crawler,SELENIUM_NEEDED,groupMapper,parser,USER_AGENT)
from .stream import StreamHandler,Error
from datetime import datetime, timedelta
import re,logging,base64,requests,os
from bs4 import BeautifulSoup
from json import loads, dumps
from django.utils import timezone



logging.basicConfig(level=logging.INFO if os.environ['DEBUG'] == "1" else logging.WARNING ,format='%(message)s')


def reboot(name_parameter):
    try:
        website = python_website_support.objects.get(website_name=name_parameter)
    except python_website_support.DoesNotExist:
        return {"error":f"{name_parameter} Does Not Exists"}

    crawl = p_non_interesting_urls.objects.filter(website_id_fk=website.id)
    crawl_success = crawl.filter(status=1).order_by("updated_at")

    if crawl_success.count():
        reboot_log.objects.create(website=website,count=crawl_success.count(),date=crawl_success[0].updated_at)

    crawl.delete()
    return {"message":f"{name_parameter} successfully rebooted"}


def scraper(name_parameter):
    """Main Scraper function that ties everything together"""
    response_json = lambda error , item : {"error":error,"item":item}
    try:
        website = python_website_support.objects.get(website_name=name_parameter)
        if not website.crawling_or_paused:
            return response_json(f"{name_parameter} is Paused, Try a Different Website!","")
        item = p_interesting_urls.objects.filter(status=0,website_id_fk=website.id)[0]
    except python_website_support.DoesNotExist:
        return response_json(f"{name_parameter} Does Not Exists","")
    except IndexError:
        if p_non_interesting_urls.objects.filter(website_id_fk=website.id,status=0).exists():
            return response_json("No Urls Found. Run the crawler to populate Urls ","")
        else:
            return response_json("No Urls Found. Reboot the website and run the crawler","")

    try:
        item.status = 3
        item.updated_at = datetime.now(tz=timezone.utc)
        item.save()

        map_json = r"{0}".format(website.map_json)
        map_json = loads(map_json, strict=False)
        #Selenium needed for scraping because html fetch syntax wont work without it
        scraped_data = parser(item.url, map_json,testing=False,selenium = name_parameter in SELENIUM_NEEDED)


        if scraped_data == 0:
            item.status = 2
            item.save()
            return response_json("Invalid Url or Missing Map Json",item.url)

        #Common preprocessing
        if "url" not in scraped_data or scraped_data.get("url") == "":
            scraped_data["url"] = item.url

        primary = scraped_data.get("title","")
        secondary = scraped_data.get("description","")

        if website.feature == "education":
            primary += " " +scraped_data.get("audience")
            secondary = ""

        if website.feature == "job":
            print("\n"*20)
            remote = False
            if scraped_data.get('job_location','').lower() in ('work from home'):
                print("inside")
                try:
                    scraped_data['job_location'] = scraped_data.pop('about_us','')
                    remote = True
                except Exception as err:
                    print(err)                  
            scraped_data['remote'] = remote


        catched_groups = groupMapper(primary,secondary,website.group_id_fk,website.exception_group_id_fk)

        handler = StreamHandler(website)
        if website.feature == "education":
            response = handler.processEducation(scraped_data,catched_groups,item)
        elif website.feature == "job":
            response = handler.processJob(scraped_data,catched_groups,item)
        elif website.feature == "event":
            response = handler.processEvent(scraped_data,catched_groups,item)

        if isinstance(response,Error):
            item.status = 2
            item.save()
            return response_json(response.show(),item.url)

        auto_id = response
        item.status = 1
        item.save()
        return {**scraped_data,"groups":catched_groups,"id":auto_id,"type":website.feature}
    except Exception as err:
        item.status = 2
        item.save()
        return response_json(str(err),item.url)


""" Main Crawling Function """
def crawler(name_parameter):
    response_json = lambda error , item : {"error":error,"item":item}
    cDate = datetime.now(tz=timezone.utc)
    try:
        requested_website = python_website_support.objects.get(website_name=name_parameter)
        if not requested_website.crawling_or_paused:
            return response_json(f"{name_parameter} is Paused, Try a Different Website!","")
        crawl_url = p_non_interesting_urls.objects.filter(status=0,website_id_fk=requested_website.id)[0]
    except python_website_support.DoesNotExist:
        return response_json(f"{name_parameter} Does Not Exists","")
    except IndexError:
        if not p_non_interesting_urls.objects.filter(website_id_fk=requested_website.id).exists():
            encodedBytes = base64.b64encode(requested_website.url.encode("utf-8"))
            ignition_url = p_non_interesting_urls.objects.create(website_id_fk=requested_website.id,url=requested_website.url,
            url_hash=str(encodedBytes, "utf-8"),feature=requested_website.feature,status=0,created_at=cDate,updated_at=cDate)
            return response_json("Initial Crawl URL added, run the crawler again!","")
        else:
            return response_json("All Crawl URLs are exhausted, reboot the website","")


    try:

        regex = re.compile(requested_website.regex[1:-1])
        crawl_url.updated_at = datetime.now(tz=timezone.utc)
        crawl_url.status = 3
        crawl_url.save()
        print(crawl_url.url)


        try:
            if name_parameter in SELENIUM_NEEDED:
                bsObj = BeautifulSoup(selenium_crawler(crawl_url.url), "html.parser")
            else:
                bsObj = BeautifulSoup(requests.get(crawl_url.url,headers=USER_AGENT).content, "html.parser")
        except requests.exceptions.SSLError:
            bsObj = BeautifulSoup(requests.get(crawl_url.url,verify=False,headers=USER_AGENT).content, "html.parser")
        except Exception as e:
            crawl_url.status = 2
            crawl_url.save()
            return response_json("Invalid Url",crawl_url.url)


        # Crawling all links
        interesting, non_interesting = set(), set()
        for links in bsObj.select("a[href]"):
            href = links.attrs["href"].strip()
            # Cleaning of urls
            if "?" in href:
                href = href[0:href.find("?")]
            #Some websites use // in the beginning
            if href.startswith("//"):
                href = "https:" + href
            #If a relative link then turn it into a full link
            elif href.startswith("/"):
                href = requested_website.url+href
                logging.info(f"Turned Relative into Absolute URL : {href}")

            re_match = re.search(regex, href)
            if re_match is not None:
                interesting.add(href)
                logging.info(f"Matched URL : {href}")
            #Only add to non interesting when its a link from the same website.
            elif href.startswith(requested_website.url) and  "#" not in href:
                non_interesting.add(href)
                logging.info(f"Non Interesting URL : {href}")
            else:
                logging.info(f"External URL : {href}")



        bulk1,bulk2 = [],[]
        updated_interesting = set()
        """ Bulks Creation and Write of interesting and non-interesting URLs """
        for links in interesting:
            encodedBytes = base64.b64encode(links.encode("utf-8"))
            new = p_interesting_urls(url = links,feature = requested_website.feature,created_at=cDate,updated_at=cDate,
            website_id_fk = requested_website.id,status = 0,url_hash = str(encodedBytes, "utf-8"))
            bulk1.append(new)
        p_interesting_urls.objects.bulk_create(bulk1, ignore_conflicts=True)

        for links in non_interesting:
            encodedBytes = base64.b64encode(links.encode("utf-8"))
            new = p_non_interesting_urls(url = links,feature = requested_website.feature,created_at=cDate,updated_at=cDate,
            website_id_fk = requested_website.id,status = 0,url_hash = str(encodedBytes, "utf-8"))
            bulk2.append(new)
        p_non_interesting_urls.objects.bulk_create(bulk2, ignore_conflicts=True)

        crawl_url.status = 1
        crawl_url.save()
        return {"url":crawl_url.url,"interesting_urls":interesting,"non_interesting":non_interesting}

    except Exception as err:
        crawl_url.status = 2
        crawl_url.save()
        return response_json(str(err),crawl_url.url)
