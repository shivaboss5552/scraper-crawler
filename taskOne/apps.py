from django.apps import AppConfig


class TaskoneConfig(AppConfig):
    name = 'taskOne'
