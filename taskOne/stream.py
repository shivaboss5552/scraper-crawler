from datetime import datetime
from .helpers import thumbnail,geoCode,dateProcessor,generateDbFields
import mysql.connector as sql
from django.shortcuts import HttpResponse
from .models import Event,Education,Job
import base64

SUPPORTED_CATEGORIES = ["job","education","event"]

ALL_FIELDS = {"job" :['@type', 'title', 'job_location', 'cmp_name', 'employment_type', 'min_exp', 'max_exp', 'description', 'profile_image', 'preferred_qualification', 'skill', 'latitude', 'longitude', 'tags', 'external_link_to_apply', 'application_deadline','phone_number', 'email_address', 'website', 'url', 'about_us', 'contact_us', 'how_to_apply', 'telephone_number'],
                "event":['@type', 'title', 'venue', 'start_date', 'start_time', 'end_date', 'end_time', 'latitude', 'longitude', 'location', 'website', 'terms_condition', 'contact_name','contact_number', 'email_address', 'description', 'cost', 'currency', 'tag', 'profile_image', 'url'],
                "education":['@type', 'title', 'url', 'cost', 'currency', 'description', 'profile_image', 'certificate', 'language', 'length', 'rating', 'teacher', 'audience', 'teacher_image']}


class Error:
    def __init__(self,message):
        self.error = message
    def show(self):
        return self.error


class StreamHandler:

    def __init__(self,website):
        
        self.today_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.ATG_USER_ID = website.atg_user_id
        self.IS_IMPORTED = str(int(website.is_imported))
        

    def processJob(self,scraped_data,catched_groups,item):
        #preprocessing
        """ Manadatory Data presence Check """
        if  not all([scraped_data.get("title"),scraped_data.get("description"),scraped_data.get("job_location")]):
            return Error(f"Title/Description/location data not found!")

        """ Detecting Job type """
        if not scraped_data.get("employment_type"):
            title_check = scraped_data.get("title").lower()
            if "intern" in title_check or "internship" in title_check:
                scraped_data["employment_type"] = "3"
            else:scraped_data["employment_type"] = "1"

        """ In case there is a wrong match for min and max exp """
        if scraped_data.get("min_exp") and scraped_data.get("max_exp"):
            if int(scraped_data.get("min_exp"))>int(scraped_data.get("max_exp")):
                scraped_data["max_exp"],scraped_data["min_exp"] = "",""

        scraped_data["profile_image"] = thumbnail(scraped_data["profile_image"], "job")

        processed_date =  dateProcessor(scraped_data["application_deadline"])
        if processed_date == 0:
            return Error(f"Date is passed!")
        scraped_data["application_deadline"] = processed_date

        if all([scraped_data["latitude"],scraped_data["longitude"]]):
            scraped_data["latitude"] = float(scraped_data["latitude"])
            scraped_data["longitude"] = float(scraped_data["longitude"])
        else:
            scraped_data["latitude"],scraped_data["longitude"] = geoCode(scraped_data["job_location"])


        def fetch(x): return scraped_data.get(x) if scraped_data.get(x) is not None else ""
        f_date = self.today_date


        #Duplicate/Similar Job Filter 
        if Job.objects.filter(cmp_name=fetch("cmp_name"), job_location=fetch("job_location"), title=fetch("title")).exists():
            return Error(f"Duplicate Content, Not Inserted!")

        #The fields from scraped data whose name exactly matches with the corresponding DB column names.
        main_fields = ['title', 'job_location', 'description','profile_image', 'preferred_qualification', 'tags', 'employment_type',
                       'application_deadline', 'phone_number', 'email_address', 'url', 'latitude', 'longitude', 'cmp_name']
        
        #Empty fields needs to be explicitly defined because the target Database schema cant take NULL.
        empty_string_fields = ['role', 'external_link_to_apply','functional_area', 'website', 'about_us', 'contact_us',
                                 'how_to_apply', 'hiring_process', 'telephone_number']
        empty_int_fields = ['cover_type', 'salary_range', 'traversed']

        all_fields = generateDbFields(scraped_data, main_fields, empty_string_fields, empty_int_fields)

        #Creating a new Job entry with Main fields + extra fields + fields that need to be defined at runtime.
        jobObj = Job(**all_fields,min=fetch("min_exp"),max=fetch("max_exp"), skill=fetch("skill")[0:250], import_url=fetch("url"),
                     status=("1" if catched_groups else "2"),imported=self.IS_IMPORTED, created_at=f_date, updated_at=f_date,
                      groups=",".join(map(str, catched_groups)), remote=int(fetch("remote")),user_id_fk = self.ATG_USER_ID)

        jobObj.save()
        return jobObj.id


    def processEvent(self,scraped_data,catched_groups,item):
        """ Manadatory Data presence Check """
        if  not all([scraped_data.get("title"),scraped_data.get("description"),scraped_data.get("location")]):
            return Error(f"Title/Description/location data not found!")

        scraped_data["profile_image"] = thumbnail(scraped_data["profile_image"], "event")

        if not scraped_data["profile_image"]:
            return Error("Profile Image Is a Manadatory Field for Events!")

        processed_date =  dateProcessor(scraped_data["start_date"])
        if processed_date == 0:
            return Error(f"Date is passed!")
        scraped_data["start_date"] = processed_date

        if all([scraped_data["latitude"],scraped_data["longitude"]]):
            scraped_data["latitude"] = float(scraped_data["latitude"])
            scraped_data["longitude"] = float(scraped_data["longitude"])
        else:
            scraped_data["latitude"],scraped_data["longitude"] = geoCode(scraped_data["location"])



        #Duplicate Check
        def fetch(x): return scraped_data.get(x) if scraped_data.get(x) is not None else ""
        f_date = self.today_date

        #Duplicate/Similar Job Filter
        if Event.objects.filter(start_date=fetch("start_date"), venue=fetch("venue"), title=fetch("title")).exists():
            return Error(f"Duplicate Content, Not Inserted!")

        #The fields from scraped data whose name exactly matches with the corresponding DB column names.
        main_fields = ['venue', 'title', 'start_date',
                       'start_time', 'end_date', 'end_time', 'latitude', 'longitude', 'location', 'terms_condition', 'contact_name',
                       'contact_number', 'email_address', 'description', 'cost', 'currency', 'tag', 'profile_image']

        #Empty fields needs to be explicitly defined because the target Database schema cant take NULL.
        empty_string_fields = ['agenda']
        empty_int_fields = ['cover_type', 'min_age','max_age', 'traversed']

        all_fields = generateDbFields(scraped_data, main_fields, empty_string_fields, empty_int_fields)

        #Creating a new Event entry with Main fields + extra fields + fields that need to be defined at runtime.
        eventObj = Event(**all_fields, import_url=fetch("url"), website=fetch("url"),
                     status=("1" if catched_groups else "2"), imported=self.IS_IMPORTED, created_at=f_date, updated_at=f_date,
                       groups=",".join(map(str, catched_groups)), user_id_fk=self.ATG_USER_ID, onlineTicket="0")

        eventObj.save()
        return eventObj.id




    def processEducation(self,scraped_data,catched_groups,item):
        
        scraped_data["profile_image"] = thumbnail(scraped_data["profile_image"], "education")

        if not scraped_data["profile_image"]:
            return Error("Profile Image Is a Manadatory Field for Education!")


        def fetch(x): return scraped_data.get(x) if scraped_data.get(x) is not None else ""
        f_date = self.today_date


        #The fields from scraped data whose name exactly matches with the corresponding DB column names.
        main_fields = [ 'title', 'cost','currency', 'contact_name',
                       'description', 'tag', 'profile_image','language','audience']

        #Empty fields needs to be explicitly defined because the target Database schema cant take NULL.
        empty_string_fields = ['contact_name',
                               'contact_number', 'email_address', 'venue', 'location', 'education_type']
        empty_int_fields = ['traversed', 'latitude', 'longitude', 'cover_type']


        all_fields = generateDbFields(scraped_data, main_fields, empty_string_fields, empty_int_fields)
        

        #Creating a new Education entry with Main fields + extra fields + fields that need to be defined at runtime.
        educationObj = Education(**all_fields, import_url=fetch("url"), website=fetch("url"),
                         status=("1" if catched_groups else "2"), imported=self.IS_IMPORTED, created_at=f_date, updated_at=f_date,
                         groups=",".join(map(str, catched_groups)), user_id_fk=self.ATG_USER_ID, onlineTicket="0", rating=round(float(fetch("rating")), 1),
                        taught_by=fetch("teacher"))

        educationObj.save()
        return educationObj.id

