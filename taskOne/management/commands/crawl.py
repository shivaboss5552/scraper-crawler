from django.core.management.base import BaseCommand, CommandError
from taskOne.helpers import automate



class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            automate(scrape=False)
        except Exception as e:
            print('Script Crashed!')
            with open('logs/crash_log.txt','a') as file:
                file.write(f'{str(e)}\n\n')
