from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from datetime import datetime, timedelta
import requests,logging,base64,boto3,shelve,os,re,pytz,time
from .models import Log,Website,Report,python_website_support,ScrapingControl
from django.core.mail import EmailMultiAlternatives
from bs4 import BeautifulSoup as bs
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from random import randint
from PIL import Image
from io import BytesIO
from json import loads,dumps
from copy import deepcopy



SELENIUM_NEEDED = ["udemy","engineering","ziprecruiter",]
USER_AGENT = {'user-agent':"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}
itimezone = pytz.timezone("Asia/Kolkata")

logging.basicConfig(level=logging.INFO if os.environ['DEBUG'] == "1" else logging.WARNING ,format='%(message)s')



def jsonTree(obj,catch):
    """Searches A json recursively for a key,value pair and returns the inner dictionary in which it is found.
        Used to scan and fetch parts of ld+json data embedded into job/event/education sites."""
    if type(obj) == dict:
        for identifier in catch:
            if identifier in obj.items():
                return obj
        else:
            for key,value in obj.items():
                    if type(value) == dict:
                        res = jsonTree(value,catch)
                        if res is not None:
                            return res
                    if type(value) == list:
                        for item in value:
                            if isinstance(item,(dict,list)):
                                res =  jsonTree(item,catch)
                                if res is not None:
                                    return res
            return None


def selenium_crawler(url):
    """ Renders the url in the selenium and returns the rendered page source.
        Used for scraping sites that dont provide the desired data on initial page load like udemy/bookmyshow etc"""

    #Used to fix a permission issue with the chromedriver
    os.chmod(os.path.join(os.getcwd(),"taskOne","driver"), 755)
    os.chmod(os.path.join(os.getcwd(),"taskOne","driver","chromedriver"), 755)

    options = Options()
    options.add_argument("user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36")
    options.headless = True
    driver = webdriver.Chrome(os.path.join(os.getcwd(),"taskOne","driver","chromedriver"), options=options)
    driver.get(url)
    time.sleep(2)
    source = driver.page_source
    logging.info(f"Selenium returned {len(source)} for {url}\n")
    driver.quit()
    return source

def thumbnail(url, feature):
    """ Creates thumbnail (in memory) using profile_image url and uploads original and thumbnail image to s3 Storage"""
    try:
        if url == "":return ""
        current = datetime.now()
        filename = f"atg-world-{current.month}{current.second}{current.year}{randint(100,999)}"
        #Tries to extract a file extension from the url.
        match = re.match(r"^.*?(?:\.(?P<extension>\w+))?$",url.split("/")[-1])

        extension = "jpeg"
        if match.group("extension") is not None:
            extension = match.group("extension")
            extension = "jpeg" if extension.lower() == "jpg" else extension

        logging.info(f'{filename}.{extension} Detected for Profile Image!\n')

        image = requests.get(url).content
        main_image = BytesIO(image)
        preview_image = BytesIO()

        #Generating Preview
        basewidth = 300
        img = Image.open(main_image)
        if img.width > 300:
            wpercent = (basewidth/float(img.size[0]))
            hsize = int((float(img.size[1])*float(wpercent)))
            img = img.resize((basewidth, hsize), Image.ANTIALIAS)
        img.save(preview_image,format=extension)
        full_file_name = f'{filename}.{extension}'

        try:
            access_key = str(base64.b64decode(
                "QUtJQVkzVVNJRlJGREFLVFUzQVM="), "utf-8")
            secret_key = str(base64.b64decode(
                "Qi9xT1A2KzBHSUNzWmJuVG1MVW00TW43ZGtiTERQdldTYnJWWVpFNg=="), "utf-8")
            s3 = boto3.client('s3', aws_access_key_id=access_key,
                              aws_secret_access_key=secret_key)
            # Base file
            main_image = BytesIO(image)
            s3.upload_fileobj(main_image, "atg-storage-s3",
                           f'assets/Frontend/images/{feature}_image/{full_file_name}',ExtraArgs={"ContentType":"image/jpeg"})
            # thumbnail
            preview_image.seek(0)
            s3.upload_fileobj(preview_image, "atg-storage-s3",
                           f'assets/Frontend/images/{feature}_image/thumb/{full_file_name}',ExtraArgs={"ContentType":"image/jpeg"})

            logging.info("Upload Successfull\n")
            return full_file_name
        except Exception as e:
            logging.info(f"Aws s3 error!{str(e)}")
            return ""

    except Exception as e:
        logging.info(f"Image Processing/Downloading Error:{str(e)}")
        return ""



def geoCode(location):
    """" Geocodes the location by using a prebuilt cache (built from previous gmap calls), 
        if not found in cache then uses Google Map api for Geocoding and adds this new data to cache"""
    #Local lat/long Cache
    gmap_local_conn = shelve.open("cache",writeback=True)
    api_log = gmap_local_conn["api_log"]
    gmap_cache = gmap_local_conn["gmap"]
    found = gmap_cache.get(location)
    if found is not None:
        logging.info(f"Local Fetch for lat/long of {location}\n")
        api_log["local"] += 1
        gmap_local_conn.close()
        return (found[0],found[1])
    else:
        logging.info(f"API Fetch for lat/long {location}\n")
        request_url = "https://maps.googleapis.com/maps/api/geocode/json"
        parameter = {"key": str(base64.b64decode(
            "QUl6YVN5QzZYWGVPa0l5c2JUZzMyLVZRRVVSbFBwM2FnQlN1Ynhv"), "utf-8"), "address": location}

        if parameter["address"] != "":
            json = requests.get(request_url, params=parameter)
            if json.status_code == 200:
                json = loads(json.content, strict=False)
                try:
                    json = json.get("results")[0].get(
                        "geometry").get("location")
                except (AttributeError, IndexError):
                    # in case location is not a valid location and geocoding fails
                    gmap_local_conn.close()
                    return (0,0)
                else:
                    gmap_lat,gmap_long = json.get("lat"),json.get("lng")
                    gmap_cache[parameter["address"]] = (gmap_lat,gmap_long)
                    api_log["remote"] += 1
                    gmap_local_conn.close()
                    return (gmap_lat,gmap_long)
            else:
                logging.info("Connection Error!")
                return (0,0)

        else:#Incase scraped location was ""
            return (0,0)



def htmlParser(lookup,BsObj,debug=False):
    """ Parses the HTML lookup syntax that comes from the map_json attribute of the supported websites.
        Arguments:Html Lookup syntax,BeautifulSoup object, Debug(True when used with testboard, False Otherwise)"""

    html_match = re.match(r'^\|\|(?P<lookup>.+?)(?:##(?P<regex>.+?))?\|\|$', lookup)
    if html_match is None:
        if not debug:
            raise Exception(f"{lookup} is not a valid Html DOM lookup syntax")
        else:
            return "Error:Invalid Html lookup syntax!"

    html_path = html_match.group("lookup")
    html_regex = (html_match.group("regex").replace("\\\\", "\\") if html_match.group("regex") is not None else None)

    escape_pattern = re.match(r'escape\((?P<escape>.+?)\)',html_path)
    if escape_pattern:
        return escape_pattern.group("escape")


    """Html Lookup syntax parser with custom attribute fetcher"""
    if ":html" in html_path:
        html_path = html_path.split(":")[0]
        try:
            html_result = str(BsObj.select(html_path)[0]).replace(
                "'", "\\'").replace('"', '\\"')
        except IndexError:
            html_result = ""

    elif re.match(r"(?P<lookup>.+?):attr\((?P<attribute>\w+)\)",html_path):
        """Test and depreciate :image tag"""
        query = re.match(r"(?P<lookup>.+?):attr\((?P<attribute>\w+)\)",html_path)
        lookup_path,lookup_attribute = query.group("lookup"),query.group("attribute")
        try:
            html_result = BsObj.select(lookup_path)[0].attrs[lookup_attribute]
        except (IndexError,KeyError):
            html_result = ""

    else:
        try:
            html_result = BsObj.select(html_path)[0].getText().strip()
        except (IndexError,ValueError):
            html_result = ""

    """Handles Regex embedded into Html lookup syntax"""
    if html_regex is not None:
        html_final_match = re.search(html_regex, html_result)
        if html_final_match is not None:
            lookup_match = " ".join(html_final_match.groups())
        else:
            lookup_match = ""
    else:
        lookup_match = html_result

    return lookup_match


  
def jsonParser(lookup,final_script,debug=False):
    """ Parses the ld+json/strcutured data lookup syntax that comes from the map_json attribute of the supported websites.
        Arguments: ld+json lookup syntax,Processed Json, Debug(True when used with testboard, False Otherwise)"""

    ld_match = re.match(r'^\|\|ld\+json\.(?P<path>.+?)(?:##(?P<regex>.+?))?\|\|$', lookup)
    if ld_match is None:
        if not debug:
            raise Exception(f"{lookup} is not a valid ld+json lookup syntax")
        else:
            return "Error:Invalid ld+json lookup syntax!"

    path = ld_match.group("path")
    regex = (ld_match.group("regex").replace("\\\\", "\\") if ld_match.group("regex") is not None else None)

    """Processes ld+json syntax"""
    pre_regex = ""
    temp = deepcopy(final_script)
    for breadcrumb in path.split("/"):
        path_match = re.match(r'^(?P<main>.+?):(?P<index>\d)$', breadcrumb)
        if temp is None:
            break
        if path_match is not None:
            if isinstance(temp,dict) and temp.get(path_match.group("main")) is not None:
                try:
                    temp = temp.get(path_match.group("main"))[int(path_match.group("index"))]
                except IndexError:
                    temp=None
            else:
                temp = None
        else:
            try:
                temp = temp.get(breadcrumb)
            except AttributeError:
                if isinstance(temp,list) and isinstance(temp[0],dict):
                    temp = temp[0].get(breadcrumb)
                elif isinstance(temp,str):temp=None

    pre_regex = (temp if temp is not None else "")

    """Handles Regex embedded into ld+json lookup syntax"""
    if type(pre_regex) == list:
        pre_regex = " ".join(pre_regex)
    elif type(pre_regex) == dict:
        pre_regex = str(pre_regex)

    if regex is not None:
        final_match = re.search(regex, pre_regex)
        if final_match is not None:
            lookup_match = " ".join(final_match.groups())
        else:
            lookup_match = ""
    else:
        lookup_match = pre_regex

    return lookup_match



def parser(targetURL, MapJson,testing=False,selenium=False):
    info = dict()
    identifier = [item.strip() for item in MapJson.pop("@type","Unspecified").strip().split(",")]
    try:
        if selenium:
            BsObj = bs(selenium_crawler(targetURL), "html.parser")
        else:
            BsObj = bs(requests.get(targetURL,headers=USER_AGENT).content, "html.parser")
    except requests.exceptions.SSLError:
        BsObj = bs(requests.get(targetURL,verify=False,headers=USER_AGENT).content, "html.parser")
    except Exception as e:
        logging.info(f"Url Processing Error in Parser {str(e)}")
        if testing:
            return {}.fromkeys(list(MapJson.keys()),"Error: Invalid URL Passed !")
        return 0


    final_script = ""
    """ Finds the embedded Json with correct @type attribute in the markup"""
    for scripts in BsObj.findAll("script", {"type": "application/ld+json"}):
        logging.info(f"Detected Script : {scripts}\n")
        logging.info(f"Cleaned Script : {scripts.getText()}\n")
        if final_script != "":
            break
        try:
            scripts = loads(scripts.getText(), strict=False)
        except Exception as e:
            """Cleans the syntax of encountered Json if initial processing fails"""
            logging.info(f"Cant convert to JSON due to {str(e)}. Trying to clean..\n")
            def clean_json(string):
                string = re.sub(",[ \t\r\n]+}", "}", string)
                string = re.sub(",[ \t\r\n]+\]", "]", string)
                return string
            try:
                scripts = loads(clean_json(scripts.getText().rstrip(";")),strict=False)
            except Exception as e:
                logging.info(f"Script dropped due to {str(e)}\n")
                continue

        res = jsonTree({"content":scripts},[("@type",item) for item in identifier])
        logging.info(f"JsonTree for {identifier} returned {res}\n")
        if res is not None:
            logging.info(f"Script found:{res}\n")
            final_script = res
            break

    logging.info(f"Final Script is this{final_script} and identifier was {identifier}\n\n")

    """ To ignore missing ld+json by detecting @type = None (complete reliance on Html Lookup) """
    if final_script == "" and identifier != ["None"]:
        if testing:
            return {}.fromkeys(list(MapJson.keys()),"Error: Couldnt find embedded JSON or Wrong URL !")
        return 0


    """Map_json Parsing(ld+json,Html)"""
    logging.info("Key Value Pairs Found:\n\n")
    for key, value in MapJson.items():
        logging.info(f"{key} : {value}\n")
        if value == "":
            info[key] = ""
            continue
        all_value = re.findall(r'\|\|.+?\|\|', value)
        if not len(all_value):
            if testing:
                info[key] = "Error:Does Not Contain Any Lookup Syntax !"
                continue
            raise Exception(f"{value} Does Not Contain Any Lookup Syntax!")

        """Implementation of OR functionality/Fallbacks for and map_json field"""
        OR_pattern = re.compile(r'\(\|\|.+?\|\|\)(OR\(\|\|.+?\|\|\))+')
        if re.match(OR_pattern,value) is not None:
            all_OR = re.findall(r"\(\|\|.+?\|\|\)",value)
            for lookup in all_OR:
                lookup = lookup[1:-1]
                if "ld+json" in lookup:
                    evaluate_OR = jsonParser(lookup,final_script,debug=testing)
                else:
                    evaluate_OR = htmlParser(lookup,BsObj,debug=testing)

                if evaluate_OR != "":
                    value=evaluate_OR
                    break
            else:
                value = ""
        else:
            """ Without OR """
            for lookup in all_value:
                if "ld+json" in lookup:
                    medium = jsonParser(lookup,final_script,debug=testing)
                else:
                    medium = htmlParser(lookup,BsObj,debug=testing)
                if testing:
                    if medium in ("Error:Invalid ld+json lookup syntax!","Error:Invalid Html Lookup syntax!"):
                        value = medium
                        break
                value = value.replace(lookup,medium)

        if testing and value == "":
            info[key] = "Warning: The above syntax did not return any data !"
        else:
            info[key] = value
    return info


def dateProcessor(passed_date):
    def convert_dates(d):
        if d == "":return ""
        #Add new date formats here to support conversion
        supported_dates = ["%a %b %d %H:%M:%S %Z %Y","%d %b %Y","%d-%m-%Y"]
        for dates in supported_dates:
            try:
                processed_date = datetime.strptime(d,dates)
                converted_date = processed_date.strftime("%Y-%m-%d")
                return converted_date
            except ValueError:
                pass
        return ""

    date_pattern = re.compile(r'^\d{4}\-\d{2}\-\d{2}$')
    match = re.match(date_pattern,passed_date)

    if passed_date and match is None:
        """ If date is not in the format of yyyy-mm-dd , then convert it """
        passed_date = convert_dates(passed_date)

    if passed_date == "":
        """+30 days if no date is provided or wrong date format is provided"""
        return (datetime.date(datetime.now()) + timedelta(days=30)).strftime("%Y-%m-%d")

    current_date = datetime.date(datetime.now())
    last_date = datetime.date(datetime.strptime(passed_date,"%Y-%m-%d"))
    """ If Date is is in the past then return 0 """
    if last_date < current_date:
        return 0
    return passed_date



def groupMapper(primary,secondary,add_groups,exclude_groups):
    def group_finder(groups, search_string):
        """ Uses Nltk Tokenization and some preprocessing to match the search text with ATG groups"""
        catched_groups = set()
        exclude = stopwords.words("english")
        cleaned_data = bs(search_string,"html.parser").getText()
        tokenized_data = filter(lambda item: item not in exclude and item.isalpha() ,word_tokenize(cleaned_data))
        final_data = " {} ".format(" ".join(tokenized_data).lower())
        search_string = final_data
        for group_id, values in groups.items():
            for x in values:
                if " "+x.lower()+" " in search_string:
                    catched_groups.add(group_id)

        return catched_groups

    """ Accessing locally cached group_data """
    groups = shelve.open("cache")["groups"]


    catched_groups = group_finder(groups, primary)

    """ If no Group is matched in title then check inside tags and description """
    if not catched_groups:
        logging.info("Couldnt Find Groups in primary text. Now Searching in Secondary text..\n")
        catched_groups = group_finder(groups, secondary)

    #group_id_fk and exception_group_id_fk addition
    logging.info(f"Groups to add : {add_groups} Groups to exclude : {exclude_groups}\n")
    if add_groups is not None:
        add_groups = {int(g) for g in add_groups.split(",") if g.isnumeric()}
        catched_groups = catched_groups.union(add_groups)
    if exclude_groups is not None:
        exclude_groups = {int(g) for g in exclude_groups.split(",") if g.isnumeric()}
        catched_groups = catched_groups - exclude_groups

    logging.info(f"Found Groups : {catched_groups}\n")
    return catched_groups - {78}


def automate(scrape=True):
    """Used to crawl/scrape websites according to the current rules i.e scraping target/daily limit etc.
        Logs all the activity in a local cache and puts it in database at midnight """

    filename ='scraping_log' if scrape else 'crawling_log'
    initialData = shelve.open(filename)
    if "all_logs" not in initialData:
        initialData["all_logs"] = {"date":"","count":0,"logs":{}}
    initialData.close()

    initialTime = time.time()
    while True:
        logData = shelve.open(filename)
        current = datetime.now(itimezone).strftime("%Y-%m-%d")
        try:
            reportCTA = shelve.open(f'generate_{"scraping" if scrape else "crawling"}_report',flag='r')
            generate_report = reportCTA['generate_report']
        finally:
            reportCTA.close()


        if logData['all_logs']['date'] != current or generate_report:
            if logData['all_logs']['date'] != '':
                #Because This Section will run on multiple servers, so its there to space apart their execution
                #Incremental Delay can Also be used!
                logging.info("Adding/Updating local data to database, Do not close the Script!")
                logging.info(f"Local Data : {logData['all_logs']}\n\n")
                time.sleep(randint(10,30))
                pastDate = datetime.strptime(logData['all_logs']['date'],"%Y-%m-%d").date()
                totalCount = logData['all_logs']['count']
                try:
                    BaseLog = Log.objects.get(date=pastDate)
                    BaseLog.count += totalCount
                    BaseLog.save()
                    logging.info("Existing Log Data Found, Updating it.\n")
                except Log.DoesNotExist:
                    BaseLog = Log.objects.create(date=pastDate,count=totalCount)
                    logging.info("Creating a new log.\n")

                all_websites = [Website(name=python_website_support.objects.get(website_name=item),logged_on=BaseLog) for item in logData['all_logs']['logs'].keys()]
                Website.objects.bulk_create(all_websites,ignore_conflicts=True)

                all_reports = []
                for item,values in logData['all_logs']['logs'].items():
                    attached_website = Website.objects.get(name=python_website_support.objects.get(website_name=item),logged_on=BaseLog)
                    for logs in values:
                        filters = {"message":logs['message'],"website":attached_website}
                        try:
                            currentReport = Report.objects.get(**filters)
                            currentReport.count += logs['count']
                            currentReport.save()
                        except Report.DoesNotExist:
                            all_reports.append(Report(**filters,count=logs['count'],debug=logs['debug']))


                Report.objects.bulk_create(all_reports)

                try:
                    reportCTA = shelve.open(f'generate_{"scraping" if scrape else "crawling"}_report')
                    reportCTA['generate_report'] = False
                finally:
                    reportCTA.close()


            logData['all_logs'] = {"date":current,"count":0,"logs":{}}

        currentData = logData['all_logs']
        logData.close()
        logging.info(f"Current Log Data : {currentData}\n\n")

        #Fetches the current controls.
        control = ScrapingControl.objects.all().first()
        if not control:
            raise Exception("No control data set!")

        identifier = 'scraping' if scrape else 'crawling'
        is_action, daily_limit, target = getattr(control,f'is_{identifier}'), getattr(control,f'{identifier}_limit'), getattr(control,identifier)
        logging.info(f"Action : {is_action} Daily Limit : {daily_limit} Target : {target}\n")

        if time.time() - initialTime > 3300:
            logging.info("Script runtime exceeded, Killing Script!")
            break

        if not is_action or ( currentData['count'] >= daily_limit and daily_limit != 0):
            logging.info("Action Closed or Daily Limit Exceeded!\n")
            time.sleep(60)
            continue

        logging.info(f"Activity : {'scrape' if scrape else 'crawl'} Current Count : {currentData['count']}\n\n")
        res = requests.get(f'http://{os.environ["HOSTNAME"]}/api/{"scrape" if scrape else "crawl"}/?name={target}')
        currentData['logs'].setdefault(target,[])

        try:
            res = res.json()
        except Exception as e:
            logging.info("Unhandled Exception Occured, Sending Mail to Admin!")
            error_mail = EmailMultiAlternatives('Automation Script Crashed!',"","schedulemessenger.web@gmail.com",["shivaboss5552@gmail.com"])
            error_mail.attach_alternative(str(res.content), "text/html")
            error_mail.send()
            time.sleep(60)
            continue

        if 'error' in res.keys():
            message = res['error']
        else:
            message = f"Successfully {'scraped' if scrape else 'crawled'}!"
            currentData['count'] += 1

        all_reports = [x['message'] for x in currentData['logs'][target]]
        if message not in all_reports:
            currentData['logs'][target].append({'message':message,'count':1,'debug':res.get('item','')})
        else:
            currentData['logs'][target][all_reports.index(message)]['count'] += 1

        try:
            finalData = shelve.open(filename)
            finalData['all_logs'] = currentData
        finally:
            finalData.close()


def generateDbFields(scraped_data, main_fields, empty_string_fields, empty_int_fields):
    """ Used to generate DB fields """
    main_fields = {}.fromkeys(main_fields, "")
    main_fields.update({key: scraped_data[key] for key in set(main_fields.keys()).intersection(scraped_data)})

    empty_fields = {**dict.fromkeys(empty_string_fields, ""), **dict.fromkeys(empty_int_fields, 0)}

    return {**main_fields,**empty_fields}
