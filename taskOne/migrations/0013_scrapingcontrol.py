# Generated by Django 2.2.17 on 2021-02-15 05:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('taskOne', '0012_auto_20200929_1209'),
    ]

    operations = [
        migrations.CreateModel(
            name='ScrapingControl',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_scraping', models.BooleanField()),
                ('is_crawling', models.BooleanField()),
                ('scraping', models.CharField(max_length=255)),
                ('crawling', models.CharField(max_length=255)),
                ('scraping_limit', models.IntegerField()),
                ('crawling_limit', models.IntegerField()),
            ],
        ),
    ]
