# Generated by Django 2.2.12 on 2020-05-19 19:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('taskOne', '0008_auto_20200519_1938'),
    ]

    operations = [
        migrations.CreateModel(
            name='reboot_log',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.IntegerField()),
                ('date', models.DateTimeField()),
                ('website', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='taskOne.python_website_support')),
            ],
        ),
    ]
