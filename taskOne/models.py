from django.db import models
from django.db.models.signals import pre_save

class P1036AdminNonInterestingUrl(models.Model):
    website_id_fk = models.IntegerField()
    url = models.TextField()
    url_hash = models.CharField(unique=True, max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    feature = models.CharField(max_length=25)
    status = models.IntegerField()
    autocrawl = models.IntegerField(db_column='autoCrawl')  # Field name made lowercase.

    class Meta:
        db_table = 'p1036_admin_non_interesting_url'


class P1036AdminNonInterestingUrl2(models.Model):
    website_id_fk = models.IntegerField()
    url = models.TextField()
    url_hash = models.CharField(unique=True, max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    feature = models.CharField(max_length=25)
    status = models.IntegerField()
    autocrawl = models.IntegerField(db_column='autoCrawl')  # Field name made lowercase.

    class Meta:
        db_table = 'p1036_admin_non_interesting_url_2'


class P1036AdminUrlCrawled(models.Model):
    website_id_fk = models.IntegerField()
    url = models.TextField()
    url_hash = models.CharField(unique=True, max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    imported = models.IntegerField(blank=True, null=True)
    feature = models.CharField(max_length=25)
    feature_id_fk = models.IntegerField(blank=True, null=True)
    status = models.IntegerField()

    class Meta:
        db_table = 'p1036_admin_url_crawled'
    def __str__(self):
        return self.url


class P1036AdminWebsitesCrawl(models.Model):
    website_name = models.CharField(max_length=255)
    website_address = models.CharField(max_length=255)
    regex = models.CharField(max_length=255)
    feature = models.CharField(max_length=255)
    api_available = models.IntegerField(db_column='API_available')  # Field name made lowercase.
    api_regex = models.CharField(max_length=1024)
    api_url = models.CharField(max_length=1024)
    req_method = models.CharField(max_length=10)
    map_json = models.TextField()
    group_id_fk = models.CharField(max_length=1024)
    iscrawled = models.IntegerField(db_column='isCrawled')  # Field name made lowercase.
    crawling_or_paused = models.IntegerField()

    class Meta:
        db_table = 'p1036_admin_websites_crawl'
    def __str__(self):
        return self.website_name


class python_website_support(models.Model):
    id = models.IntegerField(primary_key=True)
    website_name = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    regex = models.CharField(max_length=255)
    feature = models.CharField(max_length=255)
    map_json = models.TextField()
    crawling_or_paused = models.IntegerField(default=1)
    group_id_fk = models.CharField(max_length=1024,null=True,blank=True,default="")
    exception_group_id_fk = models.CharField(max_length=1024,null=True,blank=True,default="")
    #New Fields
    atg_user_id = models.CharField(max_length=100,default='455')
    is_imported = models.BooleanField(default=True)


    def __str__(self):
        return self.website_name

class reboot_log(models.Model):
    website = models.ForeignKey(python_website_support,on_delete=models.CASCADE)
    count = models.IntegerField()
    date = models.DateTimeField()

    def __str__(self):
        return f'{self.website}({self.count})'

class p_non_interesting_urls(models.Model):
    website_id_fk = models.IntegerField()
    url = models.TextField()
    url_hash = models.CharField(unique=True, max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    feature = models.CharField(max_length=25)
    status = models.IntegerField()

    def __str__(self):
        return self.url

class p_interesting_urls(models.Model):
    website_id_fk = models.IntegerField()
    url = models.TextField()
    url_hash = models.CharField(unique=True, max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    imported = models.IntegerField(blank=True, null=True)
    feature = models.CharField(max_length=25)
    feature_id_fk = models.IntegerField(blank=True, null=True)
    status = models.IntegerField()

    def __str__(self):
        return self.url


class Log(models.Model):
    date = models.DateField(primary_key=True)
    count = models.IntegerField(default=0)
    seen = models.BooleanField(default=False)

    def __str__(self):
        return str(self.date)

class Website(models.Model):
    name = models.ForeignKey(python_website_support,on_delete=models.CASCADE)
    logged_on = models.ForeignKey(Log,on_delete=models.CASCADE)

    class Meta:
        unique_together=['name','logged_on']

    def __str__(self):
        return f'{self.name}({self.logged_on})'

class Report(models.Model):
    message = models.TextField(blank=False,null=False)
    debug = models.TextField(default='')
    count = models.IntegerField(default=0)
    website = models.ForeignKey(Website,on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.message}({self.count}) for {self.website.name} on {self.website.logged_on}'


class ScrapingControl(models.Model):
    is_scraping = models.BooleanField(default=False)
    is_crawling = models.BooleanField(default=False)
    scraping    = models.CharField(max_length=255)
    crawling = models.CharField(max_length=255)
    scraping_limit = models.IntegerField(default=0)
    crawling_limit = models.IntegerField(default=0)


#Middle Database

class Education(models.Model):
    user_id_fk = models.PositiveIntegerField()
    title = models.CharField(max_length=255)
    website = models.CharField(max_length=255)
    description = models.TextField()
    contact_name = models.CharField(max_length=60, blank=True, null=True)
    contact_number = models.CharField(max_length=60, blank=True, null=True)
    email_address = models.CharField(max_length=100, blank=True, null=True)
    cost = models.CharField(max_length=255)
    currency = models.CharField(max_length=50)
    # Field name made lowercase.
    onlineticket = models.CharField(db_column='onlineTicket', max_length=1)
    profile_image = models.TextField()
    cover_type = models.IntegerField(blank=True, null=True)
    tag = models.TextField()
    venue = models.CharField(max_length=200, blank=True, null=True)
    location = models.CharField(max_length=60, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    status = models.CharField(max_length=1)
    imported = models.CharField(max_length=1)
    import_url = models.TextField(blank=True, null=True)
    education_type = models.CharField(max_length=20)
    rating = models.FloatField(blank=True, null=True)
    language = models.CharField(max_length=50)
    audience = models.TextField()
    taught_by = models.CharField(max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    groups = models.CharField(max_length=255)
    traversed = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'middle_mst_education'


class Event(models.Model):
    user_id_fk = models.PositiveIntegerField()
    title = models.CharField(max_length=255)
    venue = models.CharField(max_length=200)
    start_date = models.DateField()
    start_time = models.CharField(max_length=20)
    end_date = models.DateField(blank=True, null=True)
    end_time = models.CharField(max_length=20)
    min_age = models.IntegerField()
    max_age = models.IntegerField()
    latitude = models.FloatField()
    longitude = models.FloatField()
    location = models.TextField()
    website = models.CharField(max_length=200)
    terms_condition = models.TextField()
    contact_name = models.CharField(max_length=60)
    contact_number = models.CharField(max_length=60)
    email_address = models.CharField(max_length=100)
    agenda = models.TextField()
    description = models.TextField()
    guest = models.CharField(max_length=5)
    cost = models.CharField(max_length=255)
    currency = models.CharField(max_length=50)
    # Field name made lowercase.
    onlineticket = models.CharField(db_column='onlineTicket', max_length=1)
    tag = models.TextField()
    profile_image = models.TextField()
    cover_type = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=1)
    imported = models.CharField(max_length=1)
    import_url = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    groups = models.CharField(max_length=255)
    traversed = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'middle_mst_event'


class Job(models.Model):
    user_id_fk = models.PositiveIntegerField()
    title = models.CharField(max_length=255)
    job_location = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    profile_image = models.TextField()
    cover_type = models.IntegerField(blank=True, null=True)
    min = models.CharField(max_length=25)
    preferred_qualification = models.CharField(max_length=255)
    role = models.CharField(max_length=255)
    skill = models.CharField(max_length=255)
    tags = models.CharField(max_length=255)
    external_link_to_apply = models.CharField(max_length=255)
    functional_area = models.CharField(max_length=255)
    employment_type = models.CharField(max_length=1)
    application_deadline = models.DateField()
    salary_range = models.FloatField()
    phone_number = models.CharField(max_length=255)
    email_address = models.CharField(max_length=255)
    website = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    about_us = models.TextField(blank=True, null=True)
    contact_us = models.TextField(blank=True, null=True)
    how_to_apply = models.TextField(blank=True, null=True)
    hiring_process = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=1)
    imported = models.CharField(max_length=1)
    import_url = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    latitude = models.CharField(max_length=225)
    longitude = models.CharField(max_length=225)
    cmp_name = models.CharField(max_length=255)
    max = models.CharField(max_length=25)
    telephone_number = models.CharField(max_length=12)
    groups = models.CharField(max_length=255)
    traversed = models.IntegerField()
    remote = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'middle_mst_job'






