from django.test import SimpleTestCase
from django.urls import reverse
from .helpers import parser

MapJson = {
	"@type": "TestCase",
	"title": "||ld+json.title||", #Direct Fetch
	"blank1": "||ld+json.blank||", #Direct Fetch that does not exist.
	"job_location": "||ld+json.jobLocation/address/addressLocality||", # Levels Deep Fetch
	"employment_type": "||escape(Intern)||", # Hardcoded Data
	"max_exp": "||ld+json.experienceRequirements##\\d+.+?(\\d+)||", #Regex on ld+json
	"phone_number": "||ld+json.description##(?:\\+\\d{2}[^0-9]?)?(\\d{10})||", #Case where base data is available but regex does not match
	"email_address": "||ld+json.blank##([a-zA-Z0-9_.]+@[a-zA-Z_]+?\\.(?:com|org|in|net|co))||", #Case where base data is not available so regex can not match
	"blank2":"|| .testblock > span#blank ||", #Direct Content Fetch for HTML that does not exist
	"html1":"|| .testblock > span#heading ||", #Direct Content Fetch for HTML
	"rhtml1":"|| .testblock > span#heading##(\d+)||", #Regex Test for Direct Content Fetch for HTML
	"html2":"|| .testblock > span#heading:html||", #Html Fetch instead of text
	"rhtml2":"|| .testblock > span#heading:html##id=..(.{7})||", #Regex Test for Html Fetch instead of text
	 "html3":"|| .testblock > span#heading:attr(role)||", #Fetching a tag attribute
	 "rhtml3":"|| .testblock > span#heading:attr(role)##(\w{4})||", #Regex on Fetching a tag attribute
	"OR1": "(||div.blank:html||)OR(||ld+json.blank||)OR(||ld+json.description||)", #Test1 of OR syntax
	"OR2": "(||div.blank:html||)OR(||ld+json.title##(blank)||)OR(||ld+json.description||)", #Test2 of OR syntax,
	"OR3": "(||div.blank:html||)OR(||ld+json.description##(.*)||)OR(||ld+json.title||)", #Test3 of OR syntax,
	"OR4": "(||ld+json.title##(\d{9})||)OR(||escape(123456789)||)", #Test4 of OR syntax
	"index1":"||ld+json.address/addressRegion||", # Testing for unspecified indexing, should default to 0
	"index2":"||ld+json.address:1/addressRegion||", # Testing for an index that exists
	"index3":"||ld+json.address:1/addressRegion##(\w{5})||", # Testing Regex for an index that exists
	"index4":"||ld+json.address:1/addressRegion/blank||", # Testing for an index where only base exists
	"index5":"||ld+json.address:2/addressRegion||", # Testing for an index that Does not exist,
	"list1":"||ld+json.profile_image||", #Testing for list
	"list2":"||ld+json.profile_image:0||", #Testing for list index
	"list3":"||ld+json.profile_image##(\w{5})||", #Testing for regex on native list
	"object1":"||ld+json.object||", #Testing for object
	"object2":"||ld+json.object##(\w{3})||", #Testing for regex on native object
}

ParsedData = {
	'title': 'SAP SD Consultant - IT (8-14 yrs)',
	'blank1': 'Warning: The above syntax did not return any data !',
	'job_location': 'Anywhere in India/Multiple Locations',
	'employment_type': 'Intern',
	'max_exp': '14',
	'phone_number': 'Warning: The above syntax did not return any data !',
	'email_address': 'Warning: The above syntax did not return any data !',
	'blank2': 'Warning: The above syntax did not return any data !',
	'html1': 'This is the Text Value, id = 4854548 end',
	'rhtml1': '4854548',
	'html2': '<span id=\\"heading\\" role=\\"this is role\\"> This is the Text Value, id = 4854548 end</span>',
	'rhtml2': 'heading',
	'html3': 'this is role',
	'rhtml3': 'this',
	'OR1': '<p><p><u><b>Responsibilities:</b></u><br/><br/>Strong Communications and Good Problem Solving skills</p></p>',
	'OR2': '<p><p><u><b>Responsibilities:</b></u><br/><br/>Strong Communications and Good Problem Solving skills</p></p>',
	'OR3': '<p><p><u><b>Responsibilities:</b></u><br/><br/>Strong Communications and Good Problem Solving skills</p></p>',
	'OR4': '123456789',
	'index1': 'Islamabad',
	'index2': ' Rawalpindi',
	'index3': 'Rawal',
	'index4': 'Warning: The above syntax did not return any data !',
	'index5': 'Warning: The above syntax did not return any data !',
	'list1': 'first.jpeg second.jpeg third.jpeg',
	'list2': 'first.jpeg',
	'list3': 'first',
	'object1': "{'key': 'value'}",
	'object2': 'key'
}
class TestParser(SimpleTestCase):

    def test_map_json(self):
        target = "http://localhost:8000"+reverse("TestCase")
        response = parser(target,MapJson,testing=True)
        self.assertEqual(response,ParsedData)
