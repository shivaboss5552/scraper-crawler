from django.urls import path,re_path
from .views import (StatusManager,WebsiteManager,AjaxManager,AnalyticsManager,
                    AnalyticsTimelineManager,DurationView,ScraperView,CrawlerView,RebootView
                    ,GenerateReport)

urlpatterns = [
    path('scrape/',ScraperView.as_view(),name="scraper"),
    path('crawl/',CrawlerView.as_view(),name="crawler"),
    path('reboot/',RebootView.as_view(),name="reboot-websites"),
    path('status/',StatusManager.as_view(),name="manage-status"),
    path('fetch/',AjaxManager.as_view(),name="testboard-data"),
    path('duration/',DurationView.as_view(),name="duration-data"),
    path('generate/report/',GenerateReport.as_view(),name="generate-report"),
    re_path(r'analytics/(?P<type>\w*)',AnalyticsManager.as_view(),name="manage-analytics"),
    re_path(r'timeline/(?P<name>\w+)',AnalyticsTimelineManager.as_view(),name="manage-timeline-analytics"),
    re_path(r'websites/(?P<name>\w*)',WebsiteManager.as_view(),name="manage-status"),

]
