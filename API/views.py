from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import StatusSerialzer,WebsiteSerializer,FetchSerializer,DurationSerializer,SC_Serializer
from taskOne.models import python_website_support,p_interesting_urls,p_non_interesting_urls,reboot_log,ScrapingControl
from taskOne.stream import (SUPPORTED_CATEGORIES,ALL_FIELDS)
from collections import Counter
from datetime import date,timedelta
from django.db.models import Q
from json import loads,dumps
from bs4 import BeautifulSoup
from taskOne.helpers import  parser,SELENIUM_NEEDED
from taskOne.main import scraper,crawler,reboot
from django.db import connection
import requests
import shelve
import itertools



class WebsiteManager(APIView):
    serializer_class = WebsiteSerializer

    def get(self,request,name,*args,**kwargs):
        """If no GET parameters are passed then returns a list of all active websites, if passed then returns
            details about the respective website, No validation on map_json(default value populated)"""
        if not name:
            active_websites = list(map(lambda x:x[0],python_website_support.objects.filter(crawling_or_paused=1).values_list("website_name")))
            return Response({"active":active_websites},status=200)
        try:
            obj = python_website_support.objects.get(website_name=name)
            json = WebsiteSerializer(obj)
            return Response(json.data,status=200)
        except python_website_support.DoesNotExist:
            return Response({"error":f"No details found for '{name}' found"},status=400)

    def post(self,request,name,*args,**kwargs):
        """Only allows addition of new websites(no updating old ones). POST data should not contain map_json
            (automatically generated depending on website feature)"""
        if name:
            return Response({"error":"No parameters allowed for POST"},status=400)

        identifier = request.data.get("website_name","")
        if python_website_support.objects.filter(website_name=identifier).exists():
            return Response({"error":f"{identifier} Already exists!"},status=400)
        passed = WebsiteSerializer(data=request.data,context={"check_map_json":False})
        if passed.is_valid():
            mapped = dumps({}.fromkeys(ALL_FIELDS[passed.validated_data["feature"]],""))
            website_obj = passed.save(map_json=mapped,crawling_or_paused=1)
            json = WebsiteSerializer(website_obj)
            return Response(json.data,status=201)
        return Response({x:",".join(y) for x,y in passed.errors.items()},status=400)

    def put(self,request,name,*args,**kwargs):
        """Takes a name parameter in the url, validates map_json (all required fields available,no extra fields supplied)
            and updates the website object"""
        if name:
            return Response({"error":f"Pass the {name} in 'website_name' parameter"},status=400)
        website_name = request.data.get("website_name","")
        try:
            obj = python_website_support.objects.get(website_name=website_name)
            update_data = WebsiteSerializer(obj,data=request.data,context={"check_map_json":True})
            if update_data.is_valid():
                saved_obj = WebsiteSerializer(update_data.save())
                return Response(saved_obj.data,status=200)
            else:
                return Response(update_data.errors,status=400)
        except python_website_support.DoesNotExist:
            return Response({"error":f"No details found for '{website_name}'! "},status=400)

class ScraperView(APIView):
    serializer_class = SC_Serializer
    def get(self,request,*args,**kwargs):
        serializer = SC_Serializer(data=request.GET)
        if serializer.is_valid():
            res = scraper(serializer.validated_data["name"])
            return Response(res,status = 400 if "error" in res else 200)
        else:
            return Response(serializer.errors,status = 400)

class CrawlerView(APIView):
    serializer_class = SC_Serializer
    def get(self,request,*args,**kwargs):
        serializer = SC_Serializer(data=request.GET)
        if serializer.is_valid():
            res = crawler(serializer.validated_data["name"])
            return Response(res,status = 400 if "error" in res else 200)
        else:
            return Response(serializer.errors,status = 400)

class RebootView(APIView):
    serializer_class = SC_Serializer
    def get(self,request,*args,**kwargs):
        serializer = SC_Serializer(data=request.GET)
        if serializer.is_valid():
            res = reboot(serializer.validated_data["name"])
            return Response(res,status = 400 if "error" in res else 200)
        else:
            return Response(serializer.errors,status = 400)

class GenerateReport(APIView):
    #Check Its Working
    def get(self,request,*args,**kwargs):
        check = request.GET.get('value')
        try:
            data1 = shelve.open('generate_scraping_report')
            data1['generate_report'] = False if check else True
            data2 = shelve.open('generate_crawling_report')
            data2['generate_report'] = False if check else True
        finally:
            data1.close()
            data2.close()
            return Response({'status':bool(not check)})

class DurationView(APIView):
    serializer_class = DurationSerializer

    def post(self,request,*args,**kwargs):
        durationObj = DurationSerializer(data=request.data)
        if durationObj.is_valid():
            websiteID = python_website_support.objects.get(website_name=durationObj.validated_data["websiteName"]).id
            get_data = lambda obj : obj.objects.filter(website_id_fk=websiteID,updated_at__gte = durationObj.validated_data["initialDate"],
            updated_at__lte = durationObj.validated_data["finalDate"]).count()
            return Response({"scraping_data":get_data(p_interesting_urls),"crawling_data":get_data(p_non_interesting_urls)},status=200)
        else:
            return Response(durationObj.errors,status=400)


class AjaxManager(APIView):
    """ Takes map_json , url and returns parsed data """
    serializer_class = FetchSerializer
    def post(self,request,*args,**kwargs):
        fetch_obj = FetchSerializer(data=request.data)
        if fetch_obj.is_valid():
            dataset = parser(fetch_obj.validated_data["url"],fetch_obj.validated_data["map_json"],testing=True,selenium=fetch_obj.validated_data["website"] in SELENIUM_NEEDED)
            return Response(dataset,status=200)
        return Response(fetch_obj.errors,status=400)


class StatusManager(APIView):
    """ It is used to change the scraping & crawling target/daily-rate as well as turn it on/off """
    serializer_class = StatusSerialzer

    def get(self, request, *args, **kwargs):
        controls = ScrapingControl.objects.all().first()

        if not controls:
            return Response({"error": "No previous Control Data found!"}, status=500)
        data = StatusSerialzer(controls)
        return Response(data.data, status=200)

    def post(self, request, *args, **kwargs):
        passed = StatusSerialzer(data=request.data)
        if passed.is_valid():
            status_object = passed.save()
            return Response(passed.data, status=201)
        else:
            return Response(passed.errors, status=400)





class AnalyticsTimelineManager(APIView):
    """It provides the scraping & crawling data grouped by months for the last 6 months.
        Note: The scraping data pertains to the time when they were created and not when they were actually scraped
        """
    def get(self,request,name,*args,**kwargs):
        try:
            obj = python_website_support.objects.get(website_name=name,crawling_or_paused=1)
        except python_website_support.DoesNotExist:
            return Response({"error":f"'{name}' does not Exists or paused!"},status=400)



        current = date.today()
        last_6_months = [current.strftime("%m/%Y")]
        for _ in range(0,5):
            current = current - timedelta(days=current.day)
            last_6_months.append(current.strftime("%m/%Y"))

        def date_grouper(dataset,crawl=True):
            all_data = dataset.objects.filter(website_id_fk=obj.id).filter(status=1).order_by("-updated_at").values_list("updated_at")
            timeline = dict(Counter(list(map(lambda d:d[0].strftime("%m/%Y"),all_data))))

            if crawl:
                #Making sure that items deleted during website reboot do not affect the log.
                for logs in reboot_log.objects.filter(website=obj):
                    lumped_date = logs.date.strftime("%m/%Y")
                    timeline[lumped_date] = timeline.get(lumped_date,0) + logs.count

            grouped_data = {x:timeline.get(x,0) for x in last_6_months}
            return grouped_data

        final_data = dict(name=name)
        scraping_data,crawling_data = date_grouper(p_interesting_urls,crawl=False),date_grouper(p_non_interesting_urls)
        processed_data = [{month:{"scraping":scraping_data[month],"crawling":crawling_data[month]}} for month in last_6_months]
        final_data["data"] = processed_data
        return Response(final_data,status=200)


class AnalyticsManager(APIView):
    """It return data of scraping,crawling or both depending upon the parameter, Queries are too complex for django Format
        So raw Queries are used"""
    def get(self,request,type,*args,**kwargs):
        if type != "" and type not in ("scraper","crawler"):
            return Response({"error":f"'{type}' is not a Valid Parameter!"})
        with connection.cursor() as cursor:
            serialize = lambda total,remaining,passed,failed,last:{"total":int(total),"remaining":int(remaining),"passed":int(passed),"failed":int(failed),"last":last.strftime("%d/%m/%Y")}

            if type in ("","scraper"):
                print("SCRAPER RAN!")
                cursor.execute("""SELECT taskOne_python_website_support.website_name, COUNT(*), SUM(status=0) as not_mapped, SUM(status=1) as pass, SUM(status=2) as fail, max(taskOne_p_interesting_urls.updated_at) as last_scraped,taskOne_python_website_support.feature FROM taskOne_p_interesting_urls JOIN taskOne_python_website_support ON taskOne_p_interesting_urls.website_id_fk=taskOne_python_website_support.id and taskOne_python_website_support.crawling_or_paused = 1 GROUP BY website_id_fk;""")
                scraper = list(map(lambda item: item+(("scraping_data"),),cursor.fetchall()))
                #print(scraper)
                if type == "scraper":
                    optional_data = [{"name":name,"type":category,type:serialize(a,b,c,d,e)} for name,a,b,c,d,e,category,type in scraper]
                    return Response(optional_data,status=200)

            if type in ("","crawler"):
                print("CRAWLER RAN!")
                cursor.execute("""SELECT taskOne_python_website_support.website_name, COUNT(*), SUM(status=0) as not_mapped_crawl, SUM(status=1) as pass_crawl, SUM(status=2) as fail_crawl, max(taskOne_p_non_interesting_urls.created_at) as last_crawled ,taskOne_python_website_support.feature FROM taskOne_p_non_interesting_urls JOIN taskOne_python_website_support ON taskOne_p_non_interesting_urls.website_id_fk=taskOne_python_website_support.id and taskOne_python_website_support.crawling_or_paused = 1 GROUP BY website_id_fk;""")
                crawler = list(map(lambda item: item+(("crawling_data"),),cursor.fetchall()))
                if type == "crawler":
                    optional_data = [{"name":name,"type":category,type:serialize(a,b,c,d,e)} for name,a,b,c,d,e,category,type in crawler]
                    return Response(optional_data,status=200)

        initial = lambda x:x[0]
        dataset,json = list(sorted(scraper+crawler,key=initial)),[]
        for key,group in itertools.groupby(dataset,initial):
            group = list(group)
            if len(group) == 2:
                data={}
                data["name"] = key
                for name,total,remaining,passed,failed,last,category,type in group:
                    if type == "crawling_data":
                        #Making sure that items deleted during website reboot do not affect the log.
                        logs = reboot_log.objects.filter(website__website_name=name)
                        res = serialize(total,remaining,passed,failed,last)
                        for log in logs:
                            res["total"] += log.count
                            res["passed"] += log.count
                        data[type] = res
                    else:
                        data[type] = serialize(total,remaining,passed,failed,last)

                    data["type"] = category

                json.append(data)

        return Response(json,status=200)
