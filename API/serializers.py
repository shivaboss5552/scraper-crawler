from rest_framework import serializers
from taskOne.models import python_website_support,ScrapingControl
import shelve
import re
from json import loads
from datetime import datetime
from taskOne.stream import (SUPPORTED_CATEGORIES,ALL_FIELDS)


class WebsiteSerializer(serializers.ModelSerializer):

    class Meta:
        model = python_website_support
        exclude = ["id","atg_user_id","is_imported"]
        extra_kwargs = {"map_json":{"required":False}}

    def validate(self,data):
        passed_json,passed_feature = data.get("map_json"),data.get("feature")

        if passed_feature not in SUPPORTED_CATEGORIES:
            raise serializers.ValidationError(f'{passed_feature} is Not a Valid Website Type!')


        group_id_fk,exception_group_id_fk = data.get("group_id_fk",""),data.get("exception_group_id_fk","")
        group_pattern = re.compile(r'^(\d+(,\d+)*)?$')
        if not (re.match(group_pattern,group_id_fk) and re.match(group_pattern,exception_group_id_fk)):
            raise serializers.ValidationError("Input comma separated groups!")


        if self.context.get("check_map_json"):
            try:
                parsed_json = loads(passed_json,strict=False)
            except Exception as e:
                raise serializers.ValidationError("map_json is not a valid JSON!")

            crawling_or_paused = data.get("crawling_or_paused")
            if crawling_or_paused not in (0,1):
                raise serializers.ValidationError("Invalid Value for crawling_or_paused!")

            parsed_json = parsed_json.keys()
            template_mapping  = ALL_FIELDS
            if passed_feature in template_mapping.keys():
                benchmark = template_mapping[passed_feature]
                if set(parsed_json) != set(benchmark):
                    compare = sorted([parsed_json,benchmark],key=len,reverse=True)
                    difference = set(compare[0])-set(compare[1])
                    message = "Missing Fields" if compare[0] == benchmark else "Extra fields Passed"
                    raise serializers.ValidationError(f"{message} : ({','.join(difference)}) ")
            #remove after testing
            else:
                raise serializers.ValidationError("Invalid Website Feature!")

        return data

class SC_Serializer(serializers.Serializer):
    name = serializers.CharField()

class DurationSerializer(serializers.Serializer):
    initialDate = serializers.CharField()
    finalDate = serializers.CharField()
    websiteName = serializers.CharField()

    def validate_websiteName(self,data):
        if python_website_support.objects.filter(website_name=data).exists():
            return data
        raise serializers.ValidationError("Not a Valid Website Name!")

    def validate(self,data):
        initialDate = data.get("initialDate")
        finalDate = data.get("finalDate")

        try:
            initialDate = datetime.strptime(initialDate,"%d/%m/%Y")
            finalDate = datetime.strptime(finalDate,"%d/%m/%Y")
        except ValueError:
            raise serializers.ValidationError("Dates Should be in dd/mm/yyyy format")

        if initialDate > finalDate or (finalDate - initialDate).days not in range(0,61):
            raise serializers.ValidationError("initialdate shoudld be smaller than finalDate and max date range is 60 days!")

        data["initialDate"] = initialDate
        data["finalDate"] = finalDate
        return data

class FetchSerializer(serializers.Serializer):
    url    = serializers.CharField()
    website = serializers.CharField()
    map_json    = serializers.CharField()

    def validate_map_json(self,data):
        map_json = data
        try:
            map_json = loads(map_json,strict=False)
        except:
            raise serializers.ValidationError("map_json is not a valid JSON!")
        return map_json


class StatusSerialzer(serializers.ModelSerializer):
    class Meta:
        model = ScrapingControl
        exclude = ['id']

    def create(self,validated_data):     
        controlObj = ScrapingControl.objects.all().first()
        
        #Update the object if it exists otherwise create a new one.
        #Object count should not exceed 1
        if controlObj:
            return self.update(controlObj, validated_data)
        else:
            return ScrapingControl.objects.create(**validated_data)

    def validate(self, data):
        """Checks that scraping/crawling is set to valid websites which are slso active"""
        scraping_crawling = {data.get("scraping"), data.get("crawling")}
        active = python_website_support.objects.filter(crawling_or_paused=1).values_list("website_name")
        active = set(map(lambda x: x[0], active))

        if active.intersection(scraping_crawling) == scraping_crawling:
            return data
        else:
            raise serializers.ValidationError(
                f"Not a valid selection for scraping/crawling, Available Choices are {','.join(active)}")
